FROM alpine:latest

# WORKDIR /usr/src
RUN apk update
RUN apk --no-cache add git
RUN apk --no-cache add openssl
RUN apk --no-cache add ca-certificates
RUN openssl s_client -showcerts -connect gitlab.coanda.local:443 | sed -n '/^-----BEGIN CERT/,/^-----END CERT/p' > /usr/share/ca-certificates/gitlab.coanda.local.crt
# RUN cp /usr/share/ca-certificates/gitlab.coanda.local.crt /usr/local/share/ca-certificates/gitlab.coanda.local.crt
RUN git config --global http.sslCAInfo /usr/share/ca-certificates/gitlab.coanda.local.crt
RUN echo "gitlab.coanda.local.crt" >> /etc/ca-certificates.conf
RUN update-ca-certificates