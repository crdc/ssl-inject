# Docker Container

Complete instructions here: https://docs.gitlab.com/ee/user/packages/container_registry/

## Personal Access Token

Get a personal access token by following instructions here:
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

You want **api** scope.

## Login to gitlab docker registry

run
```bash
docker login registry.example.com -u <username> -p <token>
```

Using your Gitlab username and the access token provided from your settings page.

Build and push to docker registry:
```bash
docker build -t registry.gitlab.com/crdc/ssl-inject/sslinject:v1 .
```

```bash
docker push registry.gitlab.com/crdc/ssl-inject/sslinject:v1
```
